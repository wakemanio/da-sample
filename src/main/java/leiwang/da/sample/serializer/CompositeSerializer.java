package leiwang.da.sample.serializer;

import leiwang.da.sample.DAFormat;
import leiwang.da.sample.DAFormatWriter;

/**
 * Composite Serializer serializes Object of <code>Collection</code> or <code>array</code>
 */
public abstract class CompositeSerializer extends ComplexSerializer{
    private Class<?> type = Object.class;

    protected CompositeSerializer(DAFormat daFormat, Class<?> type) {
        super(daFormat);
        if(type != null)
            this.type = type;
    }

    @Override
    protected String getObjectClass(Object object) {
        return String.format(this.getClassType(), type.getCanonicalName());
    }

    @Override
    protected void serializeElement(Object object, DAFormatWriter writer) {
        for (Object element : this.getArray(object)) {
            Serializer serializer = daFormat.getSerializer(element);
            serializer.serialize(element, "", writer);
        }
    }

    /**
     * Either the String representation of either collection or array.
     * @return collection/array string
     */
    protected abstract String getClassType();

    /**
     * Returns a list of elements in the composite object.
     * @param composite an array or collection.
     * @return a list of elements in the composite object.
     */
    protected abstract Object[] getArray(Object composite);
}
